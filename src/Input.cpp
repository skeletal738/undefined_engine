#include "Input.hpp"
#include "Engine.hpp"
#include <iostream>
#include <memory>

Engine* Input::engine;
GLFWwindow* Input::window;
GLfloat Input::mouse_sensitivity;
bool Input::keys_down[512] = {};
bool Input::lock_mouse;
bool Input::first_update;
double Input::m_rel_x;
double Input::m_rel_y;
double Input::m_x_now;
double Input::m_y_now;
int Input::wwidth;
int Input::wheight;

void Input::key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) {
	if (action == GLFW_PRESS) {
		keys_down[key] = true;
	}
	else if (action == GLFW_RELEASE) {
		keys_down[key] = false;
	}
}

void Input::mouse_callback(GLFWwindow* window, double xpos, double ypos) {
	m_x_now = xpos;
	m_y_now = ypos;
}

double Input::get_relative_mouse_xpos()
{
	return m_rel_x;
}

double Input::get_relative_mouse_ypos()
{
	return m_rel_y;
}

void Input::initInput(GLFWwindow* window, Engine* e) {
	std::cout << "INPUT: Initializing...";
	mouse_sensitivity = 0.25f;
	Input::window = window;
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetKeyCallback(window, Input::key_callback);
	glfwSetCursorPosCallback(window, Input::mouse_callback);
	lock_mouse = false;
	engine = e;
	first_update = true;
	glfwGetWindowSize(window, &wwidth, &wheight);
	std::cout << " done!\n";
}

void Input::input_update()
{
	int ax = wwidth / 2;
	int ay = wheight / 2;
	if (first_update) {
		glfwSetCursorPos(window, ax, ay);
		first_update = false;
		return;
	}
	m_rel_x = m_x_now - ax;
	m_rel_y = m_y_now - ay;
	m_x_now = ax;
	m_y_now = ay;
	glfwSetCursorPos(window, ax, ay);
}

bool Input::is_key_down(int keycode) {
	return keys_down[keycode];
}