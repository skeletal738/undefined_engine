#include "Model.hpp"
#include "../content/contentmanager.hpp"
#include "Object.hpp"
#include "Graphics/primitives/primitives.hpp"
#include "util/vertex_indexer.hpp"
#include <glad/glad.h>
#include <iostream>
struct Model::shader_uniforms {
public:
	const GLuint lightPosLoc;
	const GLuint viewPosLoc;
	const GLuint lightAmbientLoc;
	const GLuint lightDiffuseLoc;
	const GLuint lightSpecularLoc;
	const GLuint materialSpecularLoc;
	const GLuint materialShininessLoc;
	const GLuint viewLoc;
	const GLuint projectionLoc;
	const GLuint modelLoc;
	shader_uniforms(GLuint& target) :
		lightPosLoc(glGetUniformLocation(target, "light.position")),
		viewPosLoc(glGetUniformLocation(target, "viewPos")),
		lightAmbientLoc(glGetUniformLocation(target, "light.ambient")),
		lightDiffuseLoc(glGetUniformLocation(target, "light.diffuse")),
		lightSpecularLoc(glGetUniformLocation(target, "light.specular")),
		materialSpecularLoc(glGetUniformLocation(target, "material.specular")),
		materialShininessLoc(glGetUniformLocation(target, "material.shininess")),
		viewLoc(glGetUniformLocation(target, "view")),
		projectionLoc(glGetUniformLocation(target, "projection")),
		modelLoc(glGetUniformLocation(target, "model"))
	{}
};

Model::Model(const std::shared_ptr<contentmanager> content, const std::string& mat_path, const std::string& model_path, std::shared_ptr<Object> gameObject) {
	this->game_object = gameObject;
	this->name = model_path;
	Position = glm::vec3(0.0f, 0.0f, 0.0f);
	Rotation = glm::vec3(0.0f, 0.0f, 0.0f);
	Scale = glm::vec3(1.0f, 1.0f, 1.0f);
	this->My_material = content->load_content_file<material>(mat_path.c_str());
	uniform_names = std::make_unique<shader_uniforms>(My_material->shader_program()->Program);
	this->obj_m = content->load_content_file<content_obj>(model_path.c_str());
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &vertexBuffer);
	glGenBuffers(1, &uvBuffer);
	glGenBuffers(1, &normBuffer);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	
	std::vector<glm::vec3> nVerts;
	std::vector<glm::vec2> nUvs;
	std::vector<glm::vec3> nNorms;
	std::vector<unsigned short> indices;
	und::util::vertex_indexer::indexVBO(
		obj_m->vertices,
		obj_m->uvs,
		obj_m->normals,
		indices,
		nVerts,
		nUvs,
		nNorms
		);
	indexCount = indices.size();
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * nVerts.size(), nVerts.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, normBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * nNorms.size(), nNorms.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * nUvs.size(), nUvs.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), indices.data(), GL_STATIC_DRAW);

	UpdateTransform();
}

std::size_t Model::GetIndexCount() const 
{
	return indexCount;
}

void Model::Update(GLfloat deltaTime) {
	// I really doubt this will ever need to be used.
	// To be quite honest with you, I don't know why it's still here
	// What is my motive behind hoarding? It may never be known.
}

void Model::OnRender(Camera* mainCamera, const GLuint& wwidth, const GLuint& wheight) {
	// Setting up matrices for the uniforms
	std::shared_ptr<glm::mat4> view = mainCamera->GetView();
	std::shared_ptr<glm::mat4> proj = mainCamera->GetProj();
	// Assigning the uniforms
	glUniformMatrix4fv(uniform_names->viewLoc, 1, GL_FALSE, glm::value_ptr(*view));
	glUniformMatrix4fv(uniform_names->projectionLoc, 1, GL_FALSE, glm::value_ptr(*proj));
	glUniform3fv(uniform_names->viewPosLoc, 1, &mainCamera->pos[0]);
	glUniformMatrix4fv(uniform_names->modelLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));
}
void Model::UpdateTransform() {
	modelMatrix = glm::mat4();
	modelMatrix = glm::translate(modelMatrix, game_object->Position + this->Position);
	modelMatrix = glm::rotate(modelMatrix, glm::radians(game_object->Rotation.x + this->Rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(game_object->Rotation.y + this->Rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(game_object->Rotation.z + this->Rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));
}

Model::~Model() {
}

std::string Model::Name() {
	return this->name;
}
