#pragma once

#include <string>
#include <memory>
#include <utility>
#include <vector>
#include <type_traits>
#include <iostream>
#include "DefaultFiles.hpp"
#include "content_base.hpp"
#include "../Graphics/textureobject.hpp"
#include "../Graphics/ShaderProgram.hpp"
#include "../Graphics/material.hpp"
#include "../util/util.hpp"

class contentmanager {
public:
	contentmanager();
	contentmanager(const std::string&& initial_rootpath);

	template<typename T>
	std::shared_ptr<T> load_content_file(const char* path, const bool&& always_copy = false) {
		/* 
		   This function will be passed a type of content to load and a path to the asset. The path provided will be added onto assets_root.
		   If the asset is already loaded somewhere in loaded_content, then a shared_ptr will be given to the object pointing to that asset in memory.
		   If it is not, it will be loaded and then the shared_ptr will be provided.
		   Exception on mismatched content type.
		   Caller can explicitly define if they want the asset to be copied to a special object, this may be useful in cases where an object wants to modify its asset in memory
		   Without affecting other objects.
		*/
		for(std::size_t i = 0; i < loaded_content.size(); i++) {
			if(path == loaded_content[i]->get_content_path()) {
				return std::dynamic_pointer_cast<T>(loaded_content[i]);
			}
		}
		if (!DefaultFiles::CheckForFile(path)) {
			std::cout << "CONTENTMANAGER: A file " << path << " was requested, but could not be found.\n";
			std::string wrongpath(path);
			std::string newpath;
			if (wrongpath.substr(wrongpath.find_last_of(".") + 1) == "material") {
				std::cout << "Requested file's type was a .material file\n";
				newpath = DefaultFiles::default_assets[2];
				loaded_content.emplace_back(std::make_shared<T>(newpath.c_str()));
				std::cout << "Replaced it with " << newpath << "\n";
				return std::dynamic_pointer_cast<T>(loaded_content.back());
			}
			else {
				std::cout << "Wait, what? Requested file was not a known file type. PANIC!\n";
				return nullptr;
			}
		}
		else {
			loaded_content.emplace_back(std::make_shared<T>(path));
			return std::dynamic_pointer_cast<T>(loaded_content.back());
		}
	}

	template<typename T>
	void preload_content_file(const std::string&& path) {
		// This function will load a content file into loaded_content, but will not return anything. This will allow anything to pre-load an asset for use later.
		// This will probably be most useful in pre-compiling shaders.
	}

	void unload_content_file(const std::string&& path);
	void set_assets_root_dir(const std::string&& rootpath);
private:
	std::vector<std::shared_ptr<content_base>> loaded_content;
	std::string assets_root;
};
