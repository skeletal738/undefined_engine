#pragma once
#include "../content_base.hpp"
#include <string>
#include <memory>
#include "glad/glad.h"
#include "../../util/util.hpp"

class content_texture : public content_base{
private:
	const std::string content_path;
public:
	content_texture(const std::string&& pathtotexture);
	~content_texture(){}
	GLuint the_texture;
	GLint status;
	std::string get_content_path();
};
