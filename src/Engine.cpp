#include "Engine.hpp"
#include "Input.hpp"
#include "component/Model.hpp"
#include "component/Camera.hpp"
#include "component/transform.hpp"
#include "Object.hpp"
#include <iostream>



std::vector<std::string> DefaultFiles::default_assets;
bool Engine::Run() {
	std::cout << "ENGINE: Initializing\n";
	DefaultFiles::default_assets = {
		"content/shaders/basic_unlit.fs",
		"content/shaders/basic_unlit.vs",
		"content/materials/basic_unlit.material",
		"content/textures/default.png"
	};
    if (!DefaultFiles::CheckForDefaultFiles()) {
        std::cout << "A default file was not found. Please make sure you have the following files relative to the executable directory:\n";
        for (std::string path : DefaultFiles::default_assets) {
            std::cout << path << "\n";
        }
        std::cout << "The engine will now exit.\n";
        return false;
    }
	graphics = std::make_unique<Graphics>();

	window = graphics->GetWindow();

	if (window == nullptr){
		std::cout << "Failed to get a pointer to a window, I don't know why.\n";
		return false;
	}
	
	Content = std::make_shared<contentmanager>();
	
	std::cout << "Loading default level...\n";
	LoadLevel();

	objects.emplace_back(std::make_shared<Object>(10.0f,20.0f,10.0f)); // Object containing camera must be the last one added.
	objects.back().get()->add_component<Camera>();
	objects.back().get()->add_component<transform>();

	std::cout << "Done loading default level\n";
	Input::initInput(window, this);
	std::cout << "ENGINE: Initialization finished. Loop beginning\n\n";
	while(!glfwWindowShouldClose(window)){
		this->Update();
	}
	return true;
}

void Engine::LoadLevel()
{
	for (std::size_t rows = 0; rows < 5; rows++) {
		for (std::size_t columns = 0; columns < 5; columns++) {
			objects.emplace_back(std::make_shared<Object>(rows * 2, 0.0f, columns * 2));
			objects.back().get()->add_component<Model>(Content, "content/materials/basic_lit.material", "content/models/cube.obj", objects.back());
		}
	}
	/*objects.emplace_back(std::make_shared<Object>(10.0f, 2.0f, 2.0f));
	objects.back().get()->add_component<Model>(Content, "content/materials/basic_lit.material", "content/models/suzanne.obj", objects.back());
	objects.emplace_back(std::make_shared<Object>(10.0f, 50.0f, 2.0f));
	objects.back().get()->add_component<Model>(Content, "content/materials/basic_lit.material", "content/models/daddycube.obj",objects.back());*/

	//objects.emplace_back(std::make_shared<Object>(0.0f, 0.0f, 0.0f));
	//objects.back().get()->add_component<Model>(Content, "content/materials/basic_lit.material", "content/models/smoothCylinder.obj", objects.back());
	//objects.emplace_back(std::make_shared<Object>(2.0f, 0.0f, 0.0f));
	//objects.back().get()->add_component<Model>(Content, "content/materials/basic_lit.material", "content/models/flatCylinder.obj", objects.back());
}

void Engine::Update() {
	GLfloat currentFrame = glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	curFR = 1 / deltaTime;
	glfwPollEvents();
	Input::input_update();
	for (std::shared_ptr<Object> o : objects) {
		o->update(deltaTime);
	}
	graphics->Render(*objects.back().get()->get_component_by_type<Camera>(), objects, deltaTime);
	if (Input::is_key_down(GLFW_KEY_ESCAPE)) {
		Exit();
	}
	lastFrame = currentFrame;
}

void Engine::set_current_camera(std::shared_ptr<Camera> cam) {
	current_camera = cam;
}

void Engine::Exit() {
	// TODO: Add destruction code here.
	glfwSetWindowShouldClose(window, GL_TRUE);
}
