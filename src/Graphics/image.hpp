#pragma once

#include "util/util.hpp"
#include <glad/glad.h>
#include <libpng16/png.h>
#include <string>
#include <vector>
#include <memory>
#include <iostream>
#include <stdio.h>

class image{
private:
	bool transluc;
	bool translucset;
public:
	image(const std::string& path);
	bool has_transluc();
	uint32_t width;
	uint32_t height;
	GLint format;
	std::vector<uint8_t> data;
};
