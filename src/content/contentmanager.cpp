#include "contentmanager.hpp"

contentmanager::contentmanager() {
	assets_root = "./";
}

contentmanager::contentmanager(const std::string&& initial_rootpath) {
	assets_root = initial_rootpath;
}

void contentmanager::set_assets_root_dir(const std::string&& path) {
	assets_root = path;
}