#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include "../Graphics/ShaderProgram.hpp"
#include "component/Camera.hpp"
#include "../Graphics/material.hpp"
#include "../content/contentmanager.hpp"
#include "content/content_types/content_obj.hpp"
#include "component.hpp"
#include <string>
#include <memory>
class Object; // Forward declaration of Object

class Model : public Component {
private:
	struct shader_uniforms;
	std::unique_ptr<shader_uniforms> uniform_names;
	std::string name;
	std::shared_ptr<content_obj> obj_m;
	std::shared_ptr<Object> game_object;
	glm::mat4 modelMatrix;
	std::shared_ptr<glm::mat4> viewMatrix;
	std::shared_ptr<glm::mat4> projMatrix;
	int indexCount;
public:
	Model(const std::string& name, const std::vector<GLfloat>& v);
	Model(const std::shared_ptr<contentmanager> content);
	Model(const std::shared_ptr<contentmanager> content, const std::string& mat_path);
	Model(const std::shared_ptr<contentmanager> content, const std::string& mat_path, const std::string& model_path, std::shared_ptr<Object> gameObject);
	~Model();
	glm::vec3 Position;
	glm::vec3 Rotation;
	glm::vec3 Scale;
	std::shared_ptr<material> My_material;
	GLuint vertexBuffer;
	GLuint uvBuffer;
	GLuint normBuffer;
	GLuint VAO;
	GLuint EBO;
	void HandleKeyboard(const GLint& key, const GLint& scancode, const GLint& action, const GLint& mode, const GLfloat& deltaTime) override {}
	void HandleMouse(const GLfloat& xoffset, const GLfloat& yoffset) override {}
	void Update(GLfloat deltaTime) override;
	void OnRender(Camera* mainCamera, const GLuint& wwidth, const GLuint& wheight);
	void UpdateTransform();
	std::string Name() override;
	std::size_t GetIndexCount() const;
	std::size_t GetElementCount() const;
};

