#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "content/content_base.hpp"

class Shader : content_base {
public:
	Shader(const GLchar* shaderpath, GLenum shaderType);
	GLuint compiledShader;
	std::string get_content_path() override;
private:
	const std::string content_path;
	std::string parse_shader(const std::string& in, const GLchar* shaderpath) const;
};
