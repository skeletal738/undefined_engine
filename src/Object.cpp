#include "Object.hpp"
#include "component/component.hpp"
#include "component/transform.hpp"
#include "Input.hpp"
#include <string>
#include <iostream>
Object::Object(){
	Position = glm::vec3(0.0f,0.0f,0.0f);
	Rotation = glm::vec3(0.0f,0.0f,0.0f);
	Scale = glm::vec3(1.0f,1.0f,1.0f);
	
}
Object::Object(GLfloat xpos, GLfloat ypos, GLfloat zpos){
	Position = glm::vec3(xpos,ypos,zpos);
	Rotation = glm::vec3(0.0f,0.0f,0.0f);
	Scale = glm::vec3(1.0f,1.0f,1.0f);
}

template<typename T>
std::vector<std::unique_ptr<T>> Object::get_components_by_type() {
	std::vector<std::unique_ptr<T>> components_of_T;
	for (auto& component : Components){
		std::shared_ptr<T> com = dynamic_cast<std::shared_ptr<T>>(component.get());
		if(com != nullptr)
			components_of_T.emplace_back(std::make_shared<T>(component.get()));
	}
	if (!components_of_T.empty())
		return components_of_T;
	else
		return nullptr;
}

template<typename T>
void Object::remove_component() {
	for (auto& component : Components)
	{
		T* com = dynamic_cast<T*>(component.get());
		if (com != nullptr)
		{
			//delete component;
			return;
		}
	}
}

template<typename T, typename newT, typename... Args>
void Object::change_component(newT new_component, Args&&... args) {
	for (auto& component : Components)
	{
		T* com = dynamic_cast<T*>(component.get());
		if (com != nullptr)
		{
			//delete component;
			Components.emplace_back(std::make_unique<T>(std::forward<Args>(args)...));
		}
	}
}
void Object::update(GLfloat deltaTime) {
	if (Input::is_key_down(GLFW_KEY_R)) {
		rotate(100.0f * deltaTime, 0.0f, 100.0f * deltaTime);
	}
	for (auto& component : Components) {
		component->Update(deltaTime);
	}
}
void Object::rotate(GLfloat x, GLfloat y, GLfloat z){
	this->Rotation += glm::vec3(x, y, z);
	Model* m = get_component_by_type<Model>();
	if (m != nullptr) m->UpdateTransform();
}
void Object::move(GLfloat x, GLfloat y, GLfloat z){

}
void Object::render(Camera& mainCamera, const int& wwidth, const int& wheight){
	Model* modelToDraw(get_component_by_type<Model>());
	modelToDraw->My_material->shader_program()->UseProgram();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, modelToDraw->My_material->get_texture()->the_texture);
	glUniform1i(glGetUniformLocation(modelToDraw->My_material->shader_program()->Program, "tex0"), 0);
	modelToDraw->OnRender(&mainCamera,wwidth,wheight);
	glBindVertexArray(modelToDraw->VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, modelToDraw->EBO);
	glDrawElements(GL_TRIANGLES, modelToDraw->GetIndexCount(), GL_UNSIGNED_SHORT, nullptr);
	/*glBindVertexArray(modelToDraw->VAO);
	glDrawArrays(GL_TRIANGLES, 0, modelToDraw->GetVertexCount());*/
}


