#include "util.hpp"
#include <iostream>
#include <glm/glm.hpp>
#include <glad/glad.h>

namespace und::util{
	GLuint Util::lastfb;
	GLuint Util::cfb;
	void Util::push_fb(const GLuint& fbo) {
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		cfb = fbo;
	}
	void Util::pop_fb() {
		glBindFramebuffer(GL_FRAMEBUFFER, lastfb);
		GLuint foo = lastfb; // This is so that lastfb and cfb can be swapped. I couldn't think of a name
		lastfb = cfb;
		cfb = foo;
	}
	void Util::checkglerror() {
		if(glGetError ()!= 0)
			std::cout << "Error in OpenGL: " << glGetError();
	}
	bool Util::is_near(const float& foo, const float& bar){
		return fabs(foo-bar) < 0.01f;
	}
	GLuint Util::load_texture_from_png(const char* path) { // This exists as legacy, use Graphics::image instead.
		png_byte header[8];

		FILE *fp = fopen(path, "rb"); // Open the requested texture as binary. the b is ignored on POSIX systems.
		if (fp == 0)
		{
			perror(path);
			return 0; // Failure. Requestee should check for this.
		}
		fread(header, 1, 8, fp);

		if (png_sig_cmp(header,0,8))
		{
			std::cout << "Requested texture at " << path << " is not a valid PNG file.\n";
			fclose(fp);
			return 0;
		}

		png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
		if (!png_ptr)
		{
			std::cout << "PNG:STRUCT:CREATION:FAILURE AT " << path << ".\n";
			fclose(fp);
			return 0;
		}
		png_infop info_ptr = png_create_info_struct(png_ptr);
		if(!info_ptr) {
			std::cout << "PNG:INFOPTR:CREATION:FAILURE AT " << path << ".\n";
			png_destroy_read_struct(&png_ptr, nullptr,nullptr);
			fclose(fp);
			return 0;
		}
		png_infop end_info = png_create_info_struct(png_ptr);
		if(!info_ptr) {
			std::cout << "PNG:INFOPTR:CREATION:FAILURE AT " << path << ".\n";
			png_destroy_read_struct(&png_ptr, &info_ptr,nullptr);
			fclose(fp);
			return 0;
		}

		if(setjmp(png_jmpbuf(png_ptr))){
			std::cout << "Unknown error in LibPNG.\n";
			png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
			fclose(fp);
			return 0;
		}
		png_init_io(png_ptr, fp);
		png_set_sig_bytes(png_ptr, 8);
		png_read_info(png_ptr, info_ptr);

		png_uint_32 width = png_get_image_width(png_ptr, info_ptr);
		png_uint_32 height = png_get_image_height(png_ptr, info_ptr);
		int colour_type = png_get_color_type(png_ptr, info_ptr);
		int bit_depth = png_get_bit_depth(png_ptr, info_ptr);
		png_uint_32 temp_w, temp_h;

		png_get_IHDR(png_ptr, info_ptr, &temp_w, &temp_h, &bit_depth,
			&colour_type, nullptr, nullptr, nullptr);

		if (bit_depth == 16)
		{
			#if PNG_LIBPNG_VER >= 10504
				png_set_scale_16(png_ptr);
			#else
				png_set_strip_16(png_ptr);
			#endif
		}
		if (colour_type == PNG_COLOR_TYPE_PALETTE)
		{
			png_set_palette_to_rgb(png_ptr);
		}
		if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		{
			png_set_tRNS_to_alpha(png_ptr);
		}
		if (colour_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
		{
			png_set_expand_gray_1_2_4_to_8(png_ptr);
		}
		if (colour_type == PNG_COLOR_TYPE_RGB)
		{
			png_set_filler(png_ptr, 0xFF, PNG_FILLER_AFTER);
		}

		GLint format;
		png_read_update_info(png_ptr, info_ptr);
		switch(colour_type)
		{
			case PNG_COLOR_TYPE_RGB:
				format = GL_RGB;
				break;
			case PNG_COLOR_TYPE_RGB_ALPHA: // It should always be this, but check anyway.
				format = GL_RGBA;
				break;
			default:
				std::cout << "Unknown libpng colour type at " << path  << ": "<< colour_type << "\n";
				png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
				fclose(fp);
				return 0;
		}

		int rowbytes = png_get_rowbytes(png_ptr, info_ptr);
		rowbytes += 3 - ((rowbytes-1) % 4);

		png_byte* image_data = (png_byte *)malloc(rowbytes * temp_h * sizeof(png_byte)+15);
		if(image_data == nullptr){
			std::cout << "Couldn't allocate memory for PNG data\n";
			png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
			fclose(fp);
			return 0;
		}

		png_byte** row_pointers = (png_byte **)malloc(temp_h* sizeof(png_byte *));
		if (row_pointers == nullptr){
			std::cout << "Couldn't allocate memory for PNG row pointers\n";
			png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
			free(image_data);
			fclose(fp);
			return 0;
		}

		for (std::size_t i = 0; i < temp_h; i++)
		{
			row_pointers[temp_h - 1 - i] = image_data + i * rowbytes;
		}

		png_read_image(png_ptr, row_pointers);

		GLuint GLTexture;
		glGenTextures(1, &GLTexture);
		glBindTexture(GL_TEXTURE_2D, GLTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, format, temp_w, temp_h, 0, format, GL_UNSIGNED_BYTE, image_data);
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);

		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		free(image_data);
		free(row_pointers);
		fclose(fp);
		return GLTexture;
	}
}