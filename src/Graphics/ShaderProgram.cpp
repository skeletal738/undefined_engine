#include "ShaderProgram.hpp"
#include <iostream>
struct ShaderProgram::shader_uniforms {
public:
	const GLuint lightPosLoc;
	const GLuint viewPosLoc;
	const GLuint lightAmbientLoc;
	const GLuint lightDiffuseLoc;
	const GLuint lightSpecularLoc;
	const GLuint materialSpecularLoc;
	const GLuint materialShininessLoc;
	const GLuint viewLoc;
	const GLuint projectionLoc;
	const GLuint modelLoc;
	shader_uniforms(GLuint& target) :
		lightPosLoc(glGetUniformLocation(target, "light.position")),
		viewPosLoc(glGetUniformLocation(target, "viewPos")),
		lightAmbientLoc(glGetUniformLocation(target, "light.ambient")),
		lightDiffuseLoc(glGetUniformLocation(target, "light.diffuse")),
		lightSpecularLoc(glGetUniformLocation(target, "light.specular")),
		materialSpecularLoc(glGetUniformLocation(target, "material.specular")),
		materialShininessLoc(glGetUniformLocation(target, "material.shininess")),
		viewLoc(glGetUniformLocation(target, "view")),
		projectionLoc(glGetUniformLocation(target, "projection")),
		modelLoc(glGetUniformLocation(target, "model"))
	{}
};
ShaderProgram::ShaderProgram(std::vector<Shader> shaders) {
	this->Program = glCreateProgram();
	GLint win;
	GLchar ilog[512];
	for(std::size_t i=0; i < shaders.size(); i++){
		glAttachShader(this->Program, shaders[i].compiledShader);
	}
	glLinkProgram(this->Program);
	glGetProgramiv(this->Program, GL_LINK_STATUS, &win);
	if(!win){
		glGetProgramInfoLog(this->Program, 512, nullptr, ilog);
		std::cout << "Error linking shader program: " << ilog << std::endl;
	}
	uniform_locs = std::make_unique<shader_uniforms>(Program);
}
ShaderProgram::ShaderProgram(const std::string& vshader, const std::string& fshader) {
	Shader vs = Shader(vshader.c_str(), GL_VERTEX_SHADER);
	Shader fs = Shader(fshader.c_str(), GL_FRAGMENT_SHADER);
	this->Program = glCreateProgram();
	GLint win;
	GLchar ilog[512];
	glAttachShader(Program, vs.compiledShader);
	glAttachShader(Program, fs.compiledShader);
	glLinkProgram(Program);
	glGetProgramiv(Program, GL_LINK_STATUS, &win);
	if(!win) {
		glGetProgramInfoLog(this->Program, 512, nullptr, ilog);
		std::cout << "Error linking shader program: " << ilog << std::endl;
	}
	uniform_locs = std::make_unique<shader_uniforms>(Program);
}
void ShaderProgram::UseProgram() {
	GLint i;
	glGetIntegerv(GL_CURRENT_PROGRAM, &i);
	if (this->Program != i)
		glUseProgram(this->Program);
}
void ShaderProgram::UpdateUniforms() {
	UseProgram();
	// All of this should be objectified (Light components, material properties)
	// However, uniform_locs is opaque, so figure it out.
	// Should this belong to Material, rather than ShaderProgram?
	glm::vec3 lightColor;
	lightColor.x = 1.0f;
	lightColor.y = 0.1f;
	lightColor.z = 1.0f;
	glm::vec3 diffuseColor = lightColor   * glm::vec3(0.5f);
	glm::vec3 ambientColor = diffuseColor * glm::vec3(0.2f);
	glUniform3f(uniform_locs->lightPosLoc, 0.0f, 5.0f, 0.0f);
	glUniform3fv(uniform_locs->lightAmbientLoc, 1, &ambientColor[0]);
	glUniform3fv(uniform_locs->lightDiffuseLoc, 1, &diffuseColor[0]);
	glUniform3f(uniform_locs->lightSpecularLoc, 1.0f, 1.0f, 1.0f);
	glUniform3f(uniform_locs->materialSpecularLoc, 0.5f, 0.5f, 0.5f);
	glUniform1f(uniform_locs->materialShininessLoc, 8.0f);
}

std::string ShaderProgram::get_content_path() {
	return content_path;
}

ShaderProgram::~ShaderProgram() {}