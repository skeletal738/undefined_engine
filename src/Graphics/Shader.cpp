#include "Shader.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
std::string Shader::parse_shader(const std::string& in, const GLchar* shaderpath) const {
	std::istringstream s(in);
	std::string line;
	std::ifstream incFile;
	std::string final;
	while(std::getline(s, line)){
		if(line.find("#pragma include") == std::string::npos) {
			final.append(line);
			final.append("\n");
		}
		else {
			std::size_t foo = line.find('"') + 1;
			std::size_t bar = line.rfind('"');
			std::string relIncPath = line.substr(foo, bar - foo);
			std::string shaderPath(shaderpath);
			std::string shaderDir (
				shaderPath.substr (
					0,
					shaderPath.rfind("/") + 1
				)
			);
			std::string absIncPath = shaderDir += relIncPath;
			try {
				incFile.open(absIncPath);
				std::stringstream incStream;
				incStream << incFile.rdbuf();
				incFile.close();
				final.append(incStream.str());
				final.append("\n");
			}
			catch (std::ifstream::failure e) {
				std::cout << "Error loading included shader file:\n"
						  << "File: " << absIncPath << "\n"
						  << "Referenced from: " << shaderPath << "\n";
			}
		}
		
	}
	return final;
}
Shader::Shader(const GLchar* shaderpath, GLenum shaderType) : content_path(shaderpath){
	std::string shaderSource;
	std::ifstream ShaderFile;

	try {
		ShaderFile.open(shaderpath);
		std::stringstream shaderStream;
		shaderStream << ShaderFile.rdbuf();
		ShaderFile.close();

		shaderSource = shaderStream.str();
	}
	catch (std::ifstream::failure e){
		std::cout << "Error loading shader source at: " << shaderpath << std::endl;
	}
	std::string parsed = parse_shader(shaderSource, shaderpath); // Parse the shader for includes
	const GLchar* ShaderCodeC = parsed.c_str();	

	GLint win;
	GLchar ilog[512];

	compiledShader = glCreateShader(shaderType);
	glShaderSource(compiledShader, 1, &ShaderCodeC, nullptr);
	glCompileShader(compiledShader);
	glGetShaderiv(compiledShader, GL_COMPILE_STATUS, &win);
	if(!win){
		glGetShaderInfoLog(compiledShader, 512, nullptr, ilog);
		std::cout << "\nShader compilation error, details below\n" << ilog  << "In " << shaderpath << std::endl;
	}
}

std::string Shader::get_content_path() {
	return content_path;
}