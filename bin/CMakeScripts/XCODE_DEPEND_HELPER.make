# DO NOT EDIT
# This makefile makes sure all linkable targets are
# up-to-date with anything they link to
default:
	echo "Do not invoke directly"

# Rules to remove targets that are older than anything to which they
# link.  This forces Xcode to relink the targets from scratch.  It
# does not seem to check these dependencies itself.
PostBuild.undefined_engine.Debug:
/Users/kole/undefined_engine/bin/Debug/undefined_engine:
	/bin/rm -f /Users/kole/undefined_engine/bin/Debug/undefined_engine


PostBuild.undefined_engine.Release:
/Users/kole/undefined_engine/bin/Release/undefined_engine:
	/bin/rm -f /Users/kole/undefined_engine/bin/Release/undefined_engine


PostBuild.undefined_engine.MinSizeRel:
/Users/kole/undefined_engine/bin/MinSizeRel/undefined_engine:
	/bin/rm -f /Users/kole/undefined_engine/bin/MinSizeRel/undefined_engine


PostBuild.undefined_engine.RelWithDebInfo:
/Users/kole/undefined_engine/bin/RelWithDebInfo/undefined_engine:
	/bin/rm -f /Users/kole/undefined_engine/bin/RelWithDebInfo/undefined_engine




# For each target create a dummy ruleso the target does not have to exist
