#pragma once

#include <glad/glad.h>
#include <libpng16/png.h>
#include <iostream>
#include <vector>
#include <stdio.h>

namespace und::util{
	class Util{
	private:
		static GLuint lastfb;
		static GLuint cfb;
	public:
		static GLuint load_texture_from_png(const char* path);
		static void checkglerror();
		static void push_fb(const GLuint& fbo);
		static void pop_fb();
		template<typename A, typename B>
		static void zip_vectors(const std::vector<A> &a, const std::vector<B> &b, std::vector<std::pair<A, B>> &zipped) {
			for (size_t i = 0; i<a.size(); ++i)
			{
				zipped.push_back(std::make_pair(a[i], b[i]));
			}
		}
		template<typename A, typename B>
		static void unzip_pair_vector(const std::vector<std::pair<A, B>> &zipped, std::vector<A> &a, std::vector<B> &b) {
			for (size_t i = 0; i<a.size(); i++)
			{
				a[i] = zipped[i].first;
				b[i] = zipped[i].second;
			}
		}
		static bool is_near(const float& foo, const float& bar);
	};
}
