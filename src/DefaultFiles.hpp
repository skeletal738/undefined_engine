#pragma once
#include <vector>
#include <string>
#include <sys/stat.h>
class DefaultFiles
{
public:
	static std::vector<std::string> default_assets;
	static bool CheckForDefaultFiles();
	static bool CheckForFile(const char* path);
};
