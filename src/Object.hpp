#pragma once

#include "component/Model.hpp"
#include "component/component.hpp"
#include "component/transform.hpp"
#include "component/Camera.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Graphics/ShaderProgram.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include "glad/glad.h"
#include <iostream>

class Object{
private:
	std::vector<std::unique_ptr<Component>> Components;
public:
	glm::vec3 Position;
	glm::vec3 Rotation;
	glm::vec3 Scale;
	Object();
	Object(GLfloat xpos, GLfloat ypos, GLfloat zpos);
	~Object(){}

	template<typename T>
	T* get_component_by_type() {
		for (auto& component : Components) {
			T* com = dynamic_cast<T*>(component.get());
			if (com != nullptr)
				return com;
		}
		return nullptr;
	}
	/*std::shared_ptr<T> get_component_by_type() { // Returns a shared_ptr to the first component in this object of a given type. 
		for (auto& component : Components){
			T* com = dynamic_cast<T*>(component.get());
			if (com != nullptr)
				return std::shared_ptr<T>(com);
		}
		return nullptr;
	}*/

	template<typename T>
	std::vector<std::unique_ptr<T>> get_components_by_type();

	template<typename T, typename... Args>
	void add_component(Args&&... args) {
		Components.emplace_back(std::make_unique<T>(std::forward<Args>(args)...));
		//std::cout << "New component added to object at " << this << ": " << Components.back().get()->Name() << "\n";
		return; //*dynamic_cast<T*>(Components.back().get());
	}

	template<typename T>
	void add_component() {
		Components.emplace_back(std::make_unique<T>());
		//std::cout << "New component added to object at " << this << ": " << Components.back().get()->Name() << "\n";
	}

	template<typename T>
	void remove_component();

	template<typename T, typename newT, typename... Args>
	void change_component(newT new_component, Args&&... args);

	void test(){
		Components.emplace_back(std::make_unique<transform>());
		std::cout << Components.back().get()->Name() << "\n";
	}

	void update(GLfloat deltaTime);
	void render(Camera& mainCamera, const int& wwidth, const int& wheight);
	void rotate(GLfloat x, GLfloat y, GLfloat z);
	void move(GLfloat x, GLfloat y, GLfloat z);
};
