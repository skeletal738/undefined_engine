#pragma once
#include <iostream>
#include <string>
#include "component.hpp"
#include <glad/glad.h>
class transform : public Component{
public:
	transform(){}
	~transform(){}

	std::string Name() override {return "transform";}
	void HandleMouse(const GLfloat& xoffset, const GLfloat& yoffset) override {}
	void HandleKeyboard(const GLint& key, const GLint& scancode, const GLint& action, const GLint& mode, const GLfloat& deltaTime) override {}
	void Update(GLfloat deltaTime) override {}
private:
};
