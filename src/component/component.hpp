#pragma once
#include <glad/glad.h>
#include <string>
#include <memory>

class Component {
	public:
		Component();
		virtual ~Component();
		virtual std::string Name() = 0;
		virtual void HandleMouse(const GLfloat& xoffset, const GLfloat& yoffset) = 0;
		virtual void HandleKeyboard(const GLint& key, const GLint& scancode, const GLint& action, const GLint& mode, const GLfloat& deltaTime) = 0;
		virtual void Update(GLfloat deltaTime) = 0;
		auto* get();
};