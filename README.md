<a href="https://scan.coverity.com/projects/undefined_engine">
  <img alt="Coverity Scan Build Status"
       src="https://scan.coverity.com/projects/12389/badge.svg"/>
</a>

features:

contains a grey blank window and some snarky debug messages

dependencies:

`pkg-config`

`glfw3`

`freetype2`

`libpng`

`msgpack`

and the all obvious, `cmake` and `make`

how 2 build:

1: `mkdir build && cd build`

2: `cmake ..`

3: `make`

4: `./undefined_engine`

Under Windows using Visual Studio, `GLFW3_INCLUDE_DIR`, `GLFW3_LIBRARY`, 
                                   `PNG_PNG_INCLUDE_DIR`, `PNG_LIBRARY_RELEASE`, 
                                   `PNG_LIBRARY_DEBUG`, `ZLIB_INCLUDE_DIR`, 
                                   `ZLIB_LIBRARY_RELEASE` and 
                                   `ZLIB_LIBRARY_DEBUG` must be set with CMAKE.
                                 
Further changes will be made in the future to make it automatically discover these directories based on a base directory.
