#pragma once
#include <GLFW/glfw3.h>
#include "component/Camera.hpp"
#include "component/Model.hpp"
#include "Graphics/ShaderProgram.hpp"
#include "Object.hpp"
#include <vector>
#include <memory>
#include "Graphics/ft_font.hpp"
class Graphics {
private:
	GLFWwindow* Window;
	GLint wwidth;
	GLint wheight;
	GLuint VAO;
	GLuint VBO;
	std::unique_ptr<ShaderProgram> textShader;
	std::shared_ptr<FT_Library> ftLib;
	std::shared_ptr<ft_font> ftFont;
	GLuint TextVBO;
	GLuint TextVAO;
public:
	GLFWwindow* GetWindow();
	Graphics();
	Graphics(const GLint&& window_width, const GLint&& window_height);
	void init();
	void Render(Camera& mainCamera, const std::vector<std::shared_ptr<Object>>& objects, const GLfloat& fps);
	void RenderDeferred(Camera& mainCamera, const std::vector<std::shared_ptr<Object>>& objects); // Implemented much later...
	void RenderTop(const GLfloat& fps);
};
