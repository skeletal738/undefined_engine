#pragma once

#include <vector>
#include <memory>
#include "ShaderProgram.hpp"
#include "textureobject.hpp"
#include "../content/contentmanager.hpp"
#include "../content/content_base.hpp"

class material : public content_base{
public:
	//material(std::shared_ptr<ShaderProgram>... shaders, std::shared_ptr<textureobject>... textures);
	material(const char* asset_path);
	~material(){}
	void set_shader_program(const std::shared_ptr<ShaderProgram> s);
	void add_texture(const textureobject& t);
	std::shared_ptr<ShaderProgram> shader_program();
	textureobject* get_texture();
	std::string get_content_path();
private:
	std::vector<std::shared_ptr<textureobject>> textures;
	std::shared_ptr<ShaderProgram> myshader;
	const std::string content_path;
};
