struct Material {
    sampler2D diffuse;
    vec3 specular;
    float shininess;
};

struct Light {
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoord;

uniform vec4 xColor;
uniform vec3 viewPos;
uniform Material material;
uniform Light light;
uniform sampler2D tex0;

out vec4 color;