#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <memory>
#include "Engine.hpp"

class Engine; //Forward declaration so the compiler doesn't cry
class Input {
private:
	static Engine* engine;
	static GLFWwindow* window;
	static bool keys_down[512];
	static bool lock_mouse;
	static bool first_update;
	static double m_x_now;
	static double m_y_now;
	static double m_rel_x;
	static double m_rel_y;
	static int wwidth;
	static int wheight;
public:
	static GLfloat mouse_sensitivity;
	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
	static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
	static double get_relative_mouse_xpos(); // This gets the mouse's x position relative to the middle of the screen
	static double get_relative_mouse_ypos(); // This is useful for any camera style where the mouse is not seen.
	static void initInput(GLFWwindow* window, Engine* engine);
	static void input_update();
	static bool is_key_down(int keycode);
};
