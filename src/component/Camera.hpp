#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "component.hpp"

class Camera : public Component {
public:
	Camera ( const GLfloat& x, 
		const GLfloat& y, 
		const GLfloat& z, 
		const GLfloat& yaw, 
		const GLfloat& pitch, 
		const GLfloat& roll, 
		const bool& constrainPitch,
		const GLfloat& wwidth = 1024.0f,
		const GLfloat& wheight = 768.0f );
	Camera(const GLfloat& wwidth = 1024, const GLfloat& wheight = 768);
	~Camera();
	void Move(int key, GLfloat deltaTime);
	std::shared_ptr<glm::mat4> GetView();
	std::shared_ptr<glm::mat4> GetProj();
	void HandleKeyboard(const GLint& key, const GLint& scancode, const GLint& action, const GLint& mode, const GLfloat& deltaTime) override;
	void HandleMouse(const GLfloat& xoffset, const GLfloat& yoffset) override;
	std::string Name() override;
	void Update(GLfloat deltaTime) override;
	glm::vec3 pos;
private:
	GLfloat yaw;
	GLfloat pitch;
	GLfloat roll;
	glm::vec3 Front;
	glm::vec3 Up;
	glm::vec3 Right;
	glm::vec3 worldUp;
	std::shared_ptr<glm::mat4> viewMatrix;
	std::shared_ptr<glm::mat4> projMatrix;
	bool constrainPitch;
	void UpdateVectors();
};
