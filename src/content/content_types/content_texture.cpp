#include "content_texture.hpp"

content_texture::content_texture(const std::string&& path) : content_path(path){
	the_texture = und::util::Util::load_texture_from_png(path.c_str());
	if (the_texture == 0)
		status = 0; //fail
	else
		status = 1; //win
}

std::string content_texture::get_content_path() {
	return content_path;
}