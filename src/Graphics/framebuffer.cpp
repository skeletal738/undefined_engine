#include "framebuffer.hpp"
#include "util/util.hpp"
Framebuffer::Framebuffer() {
	glGenFramebuffers(1,&fbo);
}

void Framebuffer::bind_fbo() {
	und::util::Util::push_fb(fbo);
}

void Framebuffer::clear_framebuffer() {
	und::util::Util::push_fb(fbo);
	glClearColor(0.2f,0.2f,0.2f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	und::util::Util::pop_fb();
}