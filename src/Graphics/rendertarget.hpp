#pragma once

#include <glad/glad.h>
#include <memory>
#include "framebuffer.hpp"

class rendertarget{
private:
	GLuint rtexture;
	GLuint w;
	GLuint h;
	const std::unique_ptr<Framebuffer> framebuffer;
public:
	rendertarget(const GLuint& width, const GLuint& height);
	void bind_rt();
};