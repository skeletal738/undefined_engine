#include <glad/glad.h>
#include "Graphics.hpp"
#include "Graphics/Shader.hpp"
#include "Graphics/ShaderProgram.hpp"
#include "Graphics/material.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glm/ext.hpp"
#include "component/Model.hpp"
#include <ft2build.h>
#include FT_FREETYPE_H
#include "Graphics/ft_font.hpp"
#include <iostream>
#include <algorithm>
#include "Object.hpp"
#include "util/util.hpp"

Graphics::Graphics(const GLint&& window_width, const GLint&& window_height) {
	wwidth = window_width;
	wheight = window_height;
	this->init();
}
Graphics::Graphics() {
	wwidth = 1024;
	wheight = 768;
	this->init();
}
void Graphics::init() {
	std::cout << "GRAPHICS: Initializing... ";
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // Compilation breaks on OS X without this

	Window = glfwCreateWindow(wwidth,wheight,"The Undefined Engine",nullptr,nullptr);

	glfwMakeContextCurrent(Window);

	glfwSwapInterval(1); // supposedly, cap to refresh rate.
	if(!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
	{
		throw std::runtime_error("Error loading GLAD. abort");
	}

	glViewport(0, 0, wwidth, wheight);
	glEnable(GL_DEPTH_TEST); // Enable depth testing (pretty important)
	glEnable(GL_CULL_FACE); // Enable culling (not that important)
	glCullFace(GL_BACK);
	glEnable(GL_BLEND); // Enable blending (really important)
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// Text rendering stuff: This should be made more elegant, perhaps a TextRenderer class with a batch of text objects?
	textShader = std::make_unique<ShaderProgram>("content/shaders/text.vs", "content/shaders/text.fs"); // Load the shader
	textShader->UseProgram();
	FT_Library ft; // Library load
	if (FT_Init_FreeType(&ft)){ // Initialize it
		std::cout << "GRAPHICS: Error initializing FT Library.\n";
	}
	else {
		ftLib = std::make_shared<FT_Library>(ft);
	}
	ftFont = std::make_shared<ft_font>(ftLib, "content/fonts/Arial.ttf"); // Get a font
	// Deep breath
	glGenVertexArrays(1, &TextVAO);
	glGenBuffers(1, &TextVBO);
	glBindVertexArray(TextVAO);
	glBindBuffer(GL_ARRAY_BUFFER, TextVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glm::mat4 projection = glm::ortho(0.0f, 1024.0f, 0.0f, 768.0f);
	GLuint loc = glGetUniformLocation(textShader->Program, "projection");
	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(projection));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
 	// BOI
	// End text rendering stuff
	std::cout << "done!\n";
}

GLFWwindow* Graphics::GetWindow() {
	return this->Window;
}

void Graphics::Render(Camera& mainCamera, const std::vector<std::shared_ptr<Object>>& objects, const GLfloat& fps) {
	glClearColor(0.2f,0.2f,0.2f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	std::vector<std::pair<std::shared_ptr<Object>, GLfloat>> orderedTransObjects; // Object,Distance(f)
	std::vector<std::shared_ptr<Object>> therest;
	std::vector<std::shared_ptr<Object>> allSorted;
	// Pass One (Order objects, could be done better?)
	for (std::size_t i = 0; i < objects.size(); i++) {
		Model* thisModel = objects[i]->get_component_by_type<Model>(); 
		if (thisModel != nullptr) { // If the object has a Model component
			if (thisModel->My_material->get_texture()->texture_image->has_transluc()) { 
				orderedTransObjects.emplace_back(std::make_pair(
					objects[i], glm::length2(mainCamera.pos - (
					objects[i].get()->Position + thisModel->Position)))); // If it has transparency, pair its distance from the camera with it.
			}
			else {
				therest.emplace_back(objects[i]); // If the texture doesn't have translucency, then place a pointer to it in "therest"
			}
		}
	}
	std::sort (
		std::begin(orderedTransObjects),
		std::end(orderedTransObjects),
		[&](const auto& a, const auto& b) {
			return a.second > b.second; // From start to end, if A is higher than B, put it first. Furthest object from the camera must be rendered first.
		}
	);
	allSorted.reserve(therest.size() + orderedTransObjects.size()); // Reserve enough size to fit all the pointers in
	allSorted.insert(allSorted.end(), therest.begin(), therest.end()); // Insert the rest (unordered)
	for (std::size_t i = 0; i < orderedTransObjects.size(); i++) {
		allSorted.emplace_back(orderedTransObjects[i].first); // Place the objects from the orderedTransObjects pair vector
	}

	// Pass Two (Render the ordered objects)
	for (std::size_t i = 0; i < allSorted.size(); i++){ // For each object in the new sorted vector
		allSorted[i]->render(mainCamera, wwidth, wheight); // Render them.
	}
	RenderTop(fps); // Render everything that's meant to be rendered on top of everything else. Like UI/Text
	glfwSwapBuffers(Window);
}

void Graphics::RenderTop(const GLfloat& fps) {
	textShader->UseProgram();
	glUniform3f(glGetUniformLocation(textShader->Program, "textColor"), 1.0f, 1.0f, 1.0f);
	glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(TextVAO);
    GLfloat scale = 0.5f;
    GLfloat x = 50.0f;
    GLfloat y = 100.0f;
    std::string text = "Frame Time: " + std::to_string(fps) + "s /" + " Framerate: " + std::to_string(1/fps) + "FPS";
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++) { // Lol I got to say c++ in my c++ code
    	ft_character ch = *ftFont->Characters[*c];
    	GLfloat xpos = x + ch.Bearing.x * scale;
        GLfloat ypos = y - (ch.GlyphSize.y - ch.Bearing.y) * scale;

        GLfloat w = ch.GlyphSize.x * scale;
        GLfloat h = ch.GlyphSize.y * scale;
        // Update VBO for each character
        GLfloat vertices[6][4] = {
            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }
        };
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        // Update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, TextVBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); 
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
    }
}
