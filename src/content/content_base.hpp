#pragma once
#include <string>

class content_base {
private:
	const std::string content_path;
public:
	content_base(){}
	content_base(const char* asset_path);
	virtual std::string get_content_path() = 0;
};
