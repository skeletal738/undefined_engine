#pragma once
#include <vector>
#include <string>
#include <memory>
#include "../util/util.hpp"
#include "glad/glad.h"
#include "Graphics/image.hpp"

class textureobject{
private:
	const std::string mypath;
public:
	textureobject(const std::string& texturepath);
	std::string get_path(){return mypath;}
	std::shared_ptr<image> texture_image;
	GLuint the_texture;
};
