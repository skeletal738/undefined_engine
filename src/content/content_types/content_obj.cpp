#include "content_obj.hpp"

content_obj::content_obj(const char* asset_path) : content_path(asset_path){
	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;
	FILE* file = fopen(asset_path, "r");
	if (file == nullptr) {
		std::cout << "Failed to open file at " << asset_path << "\n";
		throw std::runtime_error("Fucked up");
	}
	while (true) {
		char lineHead[128];
		int res = fscanf(file, "%s", lineHead);
		if (res == EOF) {
			break;
		}
		if (strcmp(lineHead, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHead, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHead, "vn") == 0) {
			glm::vec3 norm;
			fscanf(file, "%f %f %f\n", &norm.x, &norm.y, &norm.z);
			temp_normals.push_back(norm);
		}
		else if (strcmp(lineHead, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				throw std::runtime_error("Shitty OBJ importer can't handle your OBJ file.");
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
	}
	
	for (std::size_t v = 0; v < vertexIndices.size(); v++) {
		unsigned int vertexIndex = vertexIndices[v];
		glm::vec3 vertex;
		vertices.push_back(temp_vertices[vertexIndex - 1]);
	}
	for (std::size_t u = 0; u < uvIndices.size(); u++) {
		unsigned int uvIndex = uvIndices[u];
		glm::vec2 uv;
		uvs.push_back(temp_uvs[uvIndex - 1]);
	}
	for (std::size_t n = 0; n < normalIndices.size(); n++) {
		unsigned int normIndex = normalIndices[n];
		glm::vec3 normal;
		normals.push_back(temp_normals[normIndex - 1]);
	}
}