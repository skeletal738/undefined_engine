#pragma once
#include "Shader.hpp"
#include "content/content_base.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <vector>
class ShaderProgram {
public:
	GLuint Program;
	ShaderProgram(std::vector<Shader> shaders);
	ShaderProgram(const std::string& vshader, const std::string& fshader);
	~ShaderProgram();
	void UseProgram();
	void UpdateUniforms();
	std::string get_content_path();
private:
	const std::string content_path;
	struct shader_uniforms;
	std::unique_ptr<shader_uniforms> uniform_locs;
};