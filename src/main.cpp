#include <iostream>
#include <GLFW/glfw3.h>
#include "util/util.hpp"
#include "Engine.hpp"
#include <memory>

/*
Undefined Engine
Abandon all hope ye who enter here.
*/

static void error_callback(const int error, const char* description) {
	std::cout << "øøøøø A happening happened in GLFW: " << error << ": " << description << " øøøøø\n";
}

int main(int argc, char** argv) {
	std::cout << "\nThis is The Undefined Engine, Version Undefined.\n";
	std::cout << "The command used to run this was: " << argv[0] << " but I'm not very smart\n";
	#ifdef __linux__
		std::cout << "We are running on Linux...\n";
	#endif
	#ifdef _WIN32
		std::cout << "We are running on Win32...\n";
	#endif
	#ifdef DEBUG_BUILD
		std::cout << "ENGINE: Debug features enabled (there are none yet)\n\n";
	#endif
	glfwSetErrorCallback(error_callback);

	std::unique_ptr<Engine> engine = std::make_unique<Engine>();

	if(engine->Run()){
		std::cout << "ENGINE: Terminating under normal circumstances. At least I hope so.\n" << std::endl;
		glfwTerminate();
		return 0;	
	}
	glfwTerminate();
	std::cout << "ENGINE: Terminating abnormally. I am going to need a goddamn psychiatrist...\n" << std::endl;
	return 1;
}
