#pragma once
#include <string.h>
#include <vector>
#include <memory>
#include <glm/glm.hpp>
#include <iostream>
#include "content/content_base.hpp"

class content_obj : public content_base{
private:
	const std::string content_path;
public:
	content_obj(const char* asset_path);
	~content_obj() {}
	std::string get_content_path() override {
		return content_path;
	}
	std::size_t GetVertexCount() const {
		return vertices.size();
	}
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	std::vector<unsigned short> indices;
};
