#include "textureobject.hpp"

textureobject::textureobject(const std::string& texturepath) : mypath(texturepath){
	texture_image = std::make_shared<image>(mypath);

	glGenTextures(1, &the_texture);
	glBindTexture(GL_TEXTURE_2D, the_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, texture_image->format, texture_image->width, texture_image->height, 0, texture_image->format, GL_UNSIGNED_BYTE, texture_image->data.data());
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
}