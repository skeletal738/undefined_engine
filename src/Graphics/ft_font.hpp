#pragma once
#include <ft2build.h>
#include FT_FREETYPE_H
#include <memory>
#include <map>
#include <string>
#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <glm/glm.hpp>
struct ft_character {
	GLuint		TextureID;
	glm::ivec2	GlyphSize;
	glm::ivec2	Bearing;
	GLuint		Advance;
	ft_character(GLuint tex, glm::ivec2 siz, glm::ivec2 bear, GLuint adv) :
		TextureID(tex),
		GlyphSize(siz),
		Bearing(bear),
		Advance(adv)
	{}
};

class ft_font{
public:
	ft_font(std::shared_ptr<FT_Library> ft, const std::string& path);
	std::map<GLchar, std::shared_ptr<ft_character>> Characters;
};