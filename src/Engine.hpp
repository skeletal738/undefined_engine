#pragma once
#include "Input.hpp"
#include "Graphics.hpp"
#include "Object.hpp"
#include "component/Camera.hpp"
#include "content/contentmanager.hpp"
#include "DefaultFiles.hpp"
#include <memory>
#include <utility>
#include <vector>
#include <string>
class Input; // Forward declaration so the compiler doesn't cry

class Engine {
private:
	std::unique_ptr<Graphics> graphics;
	//std::unique_ptr<Input> input;
	GLFWwindow* window;
	std::shared_ptr<Camera> current_camera;
	Object* mouseTarget;
	GLfloat deltaTime = 0.0f;
	GLfloat lastFrame = 0.0f;
	GLfloat curFR = 0.0f;
	std::shared_ptr<contentmanager> Content;
public:
	bool Run();
	void LoadLevel();
	void Update();
	void Exit();
	void set_current_camera(std::shared_ptr<Camera> cam);
	GLuint wwidth;
	GLuint wheight;
	std::vector<std::shared_ptr<Object>> objects;
};
