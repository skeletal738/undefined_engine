#include "Camera.hpp"
#include "Input.hpp"
#include "Engine.hpp"
#include "Graphics.hpp"
#include <iostream>
Camera::Camera ( 
const GLfloat& x, 
const GLfloat& y, 
const GLfloat& z, 
const GLfloat& yaw, 
const GLfloat& pitch, 
const GLfloat& roll, 
const bool& constrainPitch,
const GLfloat& wwidth,
const GLfloat& wheight) {
	pos.x = x;
	pos.y = y;
	pos.z = z;
	this->yaw = yaw;
	this->pitch = pitch;
	this->roll = 0; // Bet'cha thought you could roll the camera? Get fucked idiot.
	this->constrainPitch = constrainPitch;
	this->worldUp = glm::vec3(0,1,0);
	this->Up = this->worldUp;
	this->viewMatrix = std::make_shared<glm::mat4>();
	this->projMatrix = std::make_shared<glm::mat4> (
		glm::perspective (
			glm::radians(75.0f), 
			static_cast<GLfloat>(wwidth) / static_cast<GLfloat>(wheight), 
			0.1f, 
			1000.0f));
	this->UpdateVectors();
}
Camera::Camera(const GLfloat& wwidth, const GLfloat& wheight) {
	pos.x = 0;
	pos.y = 0;
	pos.z = 0;
	this->yaw = 90;
	this->pitch = 0;
	this->roll = 0;
	this->constrainPitch = true;
	this->worldUp = glm::vec3(0,1,0);
	this->Up = this->worldUp;
	this->viewMatrix = std::make_shared<glm::mat4>();
	this->projMatrix = std::make_shared<glm::mat4> (
		glm::perspective (
			glm::radians(75.0f), 
			static_cast<GLfloat>(wwidth) / static_cast<GLfloat>(wheight), 
			0.1f, 
			1000.0f));
	this->UpdateVectors();
}
Camera::~Camera() {

}
void Camera::UpdateVectors() {
	glm::vec3 front;
	front.x = cos(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
	front.y = sin(glm::radians(this->pitch));
	front.z = sin(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));

	this->Front = glm::normalize(front);
	this->Right = glm::normalize(glm::cross(this->Front, this->worldUp));
	this->Up     = glm::normalize(glm::cross(this->Right, this->Front));
	*viewMatrix = glm::lookAt(this->pos, this->pos + this->Front, this->Up);
}

std::shared_ptr<glm::mat4> Camera::GetView() {
	return viewMatrix;
}
std::shared_ptr<glm::mat4> Camera::GetProj() {
	return projMatrix; 	
}
void Camera::Update(GLfloat deltaTime) {
	GLfloat Velocity;
	if (Input::is_key_down(GLFW_KEY_LEFT_SHIFT)) {
		Velocity = 10.0f * deltaTime;
	}
	else {
		Velocity = 3.0f * deltaTime;
	}
	if (Input::is_key_down(GLFW_KEY_W)) {
		this->pos += this->Front * Velocity;
	}
	if (Input::is_key_down(GLFW_KEY_S)) {
		this->pos -= this->Front * Velocity;
	}
	if (Input::is_key_down(GLFW_KEY_A)) {
		this->pos -= this->Right * Velocity;
	}
	if (Input::is_key_down(GLFW_KEY_D)) {
		this->pos += this->Right * Velocity;
	}
	if (Input::is_key_down(GLFW_KEY_Q)) {
		this->pos += this->worldUp * Velocity;
	}
	if (Input::is_key_down(GLFW_KEY_E)) {
		this->pos -= this->worldUp * Velocity;
	}
	double mrelx = Input::get_relative_mouse_xpos();
	double mrely = Input::get_relative_mouse_ypos();
	if (mrelx != 0) {
		yaw += mrelx * Input::mouse_sensitivity;
	}
	if (mrely != 0) {
		pitch -= mrely * Input::mouse_sensitivity;
	}
	if (constrainPitch && pitch > 88.0f)
		pitch = 88.0f;
	if (constrainPitch && pitch < -88.0f)
		pitch = -88.0f;
	UpdateVectors();
}

void Camera::HandleKeyboard(const GLint& key, const GLint& scancode, const GLint& action, const GLint& mode, const GLfloat& deltaTime) {

}
void Camera::HandleMouse(const GLfloat& xoffset, const GLfloat& yoffset) {
}

std::string Camera::Name() {
	return "Camera";
}