#pragma once
#include <vector>
#include <glad/glad.h>
class primitives{
public:
	static const std::vector<GLfloat> primitive_cube;
	static const std::vector<GLfloat> primitive_plane;
	static void init();
};