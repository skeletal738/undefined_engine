#pragma once 

#include <glad/glad.h>

class Framebuffer {
private:
	GLuint fbo;
public:
	Framebuffer();
	void bind_fbo();
	void clear_framebuffer();
	
};