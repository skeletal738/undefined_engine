#version 330 core

#pragma include "include/common.vs"

void main()
{
    FragPos = vec3(model * vec4(position,1.0));
    Normal = mat3(transpose(inverse(model))) * aNormal;
    gl_Position = projection * view * model * vec4(position, 1.0f);
    TexCoord = vec2(texCoord.x, 1.0 - texCoord.y);
}

