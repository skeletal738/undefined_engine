#include "material.hpp"
#include "Engine.hpp"
#include "Shader.hpp"
#include "ShaderProgram.hpp"
#include "textureobject.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>

material::material(const char* asset_path) :  content_path(asset_path){
	std::ifstream file(asset_path);

	if(file.fail())
	{
		std::cout << "Failed to open material file at " << asset_path << "\n";
		return;
	}
	std::string filecontent;
	std::string fshader;
	std::string vshader;
	std::string texturepath;

	while (std::getline(file, filecontent)){
		std::size_t firstq = filecontent.find("'");
		std::size_t secondq = filecontent.find_last_of("'");
		if(firstq != std::string::npos && secondq != std::string::npos){
			if(filecontent.find("vshader=") != std::string::npos){
				vshader = filecontent.substr(firstq + 1, (secondq - firstq) - 1);
			}
			if(filecontent.find("fshader=") != std::string::npos){
				fshader = filecontent.substr(firstq + 1, (secondq - firstq) - 1);
			}
			if(filecontent.find("texture=") != std::string::npos){
				texturepath = filecontent.substr(firstq + 1, (secondq - firstq) - 1);
			}
		}
	}
	if (vshader.empty() || fshader.empty() || texturepath.empty()){
		std::cout << "Material file at " << asset_path << " is not a valid material file!\n";
		return;
	}
	std::string prefix = "content/shaders/";
	std::shared_ptr<Shader> vshaderobj = std::make_shared<Shader>((prefix + vshader).c_str(), GL_VERTEX_SHADER);
	std::shared_ptr<Shader> fshaderobj = std::make_shared<Shader>((prefix + fshader).c_str(), GL_FRAGMENT_SHADER);
	std::vector<Shader> shaders;
	shaders.emplace_back(*vshaderobj);
	shaders.emplace_back(*fshaderobj);
	std::shared_ptr<ShaderProgram> shaderprogram = std::make_shared<ShaderProgram>(shaders);
	this->set_shader_program(shaderprogram);
	std::shared_ptr<textureobject> t = std::make_shared<textureobject>(texturepath);
	textures.emplace_back(t);
	myshader->UpdateUniforms();
}
std::string material::get_content_path() {
	return content_path;
}
void material::set_shader_program(const std::shared_ptr<ShaderProgram> s) {
	myshader = s;
}
void material::add_texture(const textureobject& t){
	textures.emplace_back(std::make_shared<textureobject>(t));
}
std::shared_ptr<ShaderProgram> material::shader_program(){
	return myshader;
}
textureobject* material::get_texture() {
	return textures.back().get();
}