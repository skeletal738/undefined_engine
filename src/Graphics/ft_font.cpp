#include "ft_font.hpp"
#include <iostream>
#include <string>
#include <memory>

ft_font::ft_font(std::shared_ptr<FT_Library> ft, const std::string& path) {
	// We start off by allocating the FT_Face so we can pass it to FT_New_Face
	FT_Face face;
	if(FT_New_Face(*ft, path.c_str(), 0, &face)) { // Why the &*? The & means to dereference the pointer 
		// and then the * constructs a raw pointer to that. This is required because FT_New_Face expects a raw pointer.
		std::cout << "Error loading TTF Font at: " << path << "\n";
	}
	//FT_Set_Char_Size(face, 14, 14, 0 , 0);
	FT_Set_Pixel_Sizes(face, 0, 48);
	// Now the face is loaded into its pointer, let's populate the ft_character map.
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction.

	for (GLubyte c = 0; c < 128; c++) {
		GLuint ret;
		if ((ret = FT_Load_Char(face, c, FT_LOAD_RENDER))) {
			std::cout << "Error loading character " << c << " Of font " << ret << "\n";
			continue; // Go straight to the next character
		}
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer
		);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    	std::shared_ptr<ft_character> Character = std::make_shared<ft_character> (
    		texture,
    		glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
        	glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
        	face->glyph->advance.x
        );
        Characters.insert(std::pair<GLchar, std::shared_ptr<ft_character>>(c, Character));
	}
	FT_Done_Face(face);
}