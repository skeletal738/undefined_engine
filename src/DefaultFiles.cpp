#include "DefaultFiles.hpp"

bool DefaultFiles::CheckForDefaultFiles() {
	for (std::string& path : DefaultFiles::default_assets) {
		struct stat buffer;
		if (stat(path.c_str(), &buffer) != 0) {
			return false;
		}
	}
	return true;
}
bool DefaultFiles::CheckForFile(const char* path) {
	struct stat buffer;
	return (stat(path, &buffer) == 0);
}